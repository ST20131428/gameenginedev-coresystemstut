#include "StringHasher.hpp"


StringHasher StringHasher::hasher;

StringHasher::StringHasher()
{

}

StringHasher::~StringHasher()
{

}

// static
StringHasher& StringHasher::getInstance()
{
	return hasher;
}


/* hash: form hash value for string s */
unsigned StringHasher::hash(string inString)
{
	  const char *s = inString.c_str();
    unsigned hashval;
    for (hashval = 0; *s != '\0'; s++)
      hashval = *s + 31 * hashval;
    return hashval % HASHSIZE;
}

/* lookup: look for s in hashtab */
nlist* StringHasher::lookup(string s)
{
    struct nlist *np;
    for (np = hashtab[hash(s)]; np != NULL; np = np->next)
        if (s == np->name)
          return np; /* found */
    return NULL; /* not found */
}

/* install: put name in hashtab */
nlist* StringHasher::install(string name)
{
    nlist *np;
    unsigned hashval;

		if ((np = lookup(name))==NULL)
		{ /* not found */
        np = (struct nlist*) malloc(sizeof(*np));
        if (np == NULL)
          return NULL;
				np->name = name;
        hashval = hash(name);

				//Something clever has gone on here?
				//This doesn't look right to me
				//should be putting it on the end of
				//the linked list. 
        np->next = hashtab[hashval];
        hashtab[hashval] = np;
    }

		return np;
}
